const sinon = require('sinon');

var AuthComponent = require('../index');
var expect = require('expect.js');

describe('AuthComponent', function() {
    var jwt;
    var request;

    before(() => {
        request = require('request');
        jwt      = require('jsonwebtoken');
    });

    describe('#verify()', function(){

        it('should fail on an invalid jwt', function(done){

            var expectedError = 'ANY_ERROR';

            jwtStub = sinon.stub(jwt, 'verify');
            jwtStub.callsArgWith(3, expectedError, null);

            AuthComponent.verify('ANY_JWT')
            .then(function(success){
                expect().fail();
            })
            .catch(function(error){
                expect(error).to.eql(expectedError);
                jwtStub.restore();
                done()
            });
        });

        it('should return jwt data', function(done){

            expectedDecoded = 'any decoded data';

            jwtStub = sinon.stub(jwt, 'verify');
            jwtStub.callsArgWith(3, null, expectedDecoded);

            AuthComponent.verify('ANY_JWT')
            .then(function(success){
                expect(success).to.eql(expectedDecoded);
                jwtStub.restore();
                done()
            })
            .catch(function(error){
                expect().fail();
            });
        });
    });

    describe('#authenticate()', function() {

        it('should fail on an invalid authentication request', function(done){

            var expectedError = 'ANY_ERROR';

            var requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, expectedError, null, null);

            AuthComponent.authenticate('ANY_CREDS')
            .then(function(success){
                expect().fail();
            })
            .catch(function(error){
                expect(error).to.eql(expectedError);
                requestStub.restore();
                done()
            });
        });

        it('should fail on a statusCode different than 200', function(done){

            var expectedError = 'ANY_ERROR';

            var requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, null, {statusCode: 429}, expectedError);

            AuthComponent.authenticate('ANY_CREDS')
            .then(function(success){
                expect().fail();
            })
            .catch(function(error){
                expect(error).to.eql(expectedError);
                requestStub.restore();
                done()
            });
        });

        it('should return the JWT data', function(done){

            var expectedUserJWT = 'ANY_USER_JWT';

            requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, null, {statusCode: 200}, expectedUserJWT);

            AuthComponent.authenticate('ANY_CREDS')
            .then(function(success){
                expect(success).to.eql(expectedUserJWT);
                requestStub.restore();
                done();
            })
            .catch(function(error){
                expect().fail(error)
            });
        });
    });

    describe('#createUser', function() {

        it('should return an error if request failed', function(done){

            var expectedError = 'ANY_ERROR';

            jwtStub = sinon.stub(jwt, 'sign');
            jwtStub.returns('ANY_JWT');

            requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, expectedError, null, null);

            AuthComponent.createUser(null, null)
            .then(function(success){
                expect().fail();
            })
            .catch(function(error){
                expect(error).to.eql(expectedError);
                jwtStub.restore();
                requestStub.restore();
                done()
            });
        });

        it('should return an error if the user is already created', function(done) {

            var expectedError = 'User is already created';

            jwtStub = sinon.stub(jwt, 'sign');
            jwtStub.returns('ANY_JWT');

            requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, null, {statusCode: 999}, expectedError);

            AuthComponent.createUser(null, null)
            .then(function(success){
                expect().fail()
            })
            .catch(function(error){
                expect(error).to.eql(expectedError);
                jwtStub.restore();
                requestStub.restore();
                done();
            });
        });

        it('should return a JWT DATA if everything is ok', function(done){

            var expectedUserJWT = 'ANY_USER_JWT';

            jwtStub = sinon.stub(jwt, 'sign');
            jwtStub.returns('ANY_JWT');

            requestStub = sinon.stub(request, 'post');
            requestStub.callsArgWith(1, null, { statusCode:200 }, expectedUserJWT);

            AuthComponent.createUser(null, null)
            .then(function(success){
                expect(success).to.eql(expectedUserJWT);
                jwtStub.restore();
                requestStub.restore();
                done();
            })
            .catch(function(error){
                expect().fail(error)
            });
        });
    });
});
