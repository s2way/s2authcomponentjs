request = require('request');
jwt = require('jsonwebtoken');

var AuthComponent = (function () {

    var CLOUD_FUNCTIONS_URL = null;
    var appKey = null;
    var requester = null;
    var key;

    return {
        getKey: function() {
            return new Promise(function(resolve, reject) {
                request(AuthComponent.CLOUD_FUNCTIONS_URL + '/pubkey', function (error, response, body) {
                    if (error) return reject();
                    AuthComponent.key = body;
                    resolve();
                });
            });
        },

        setup: function(options) {
            this.CLOUD_FUNCTIONS_URL = options.cloud_functions_url;
            this.appKey = options.app_key;
            this.requester = options.requester;

            return AuthComponent.getKey();
        },

        verify: function(jwtToken) {
            var verifyOptions = {
                format: 'PKCS8',
                algorithms: ['RS256']
            };

            return new Promise(function(resolve, reject){
                jwt.verify(jwtToken, AuthComponent.key, verifyOptions, function(error, decoded){
                    if (error != null ) {
                        return reject(error);
                    }
                    resolve(decoded);
                });
            });
        },

        authenticate: function(creds) {

            var requestOptions = {
                uri: AuthComponent.CLOUD_FUNCTIONS_URL + '/authenticate',
                json: true,
                headers: {
                    'Authorization' : 'Basic ' + creds
                }
            };

            return new Promise(function(resolve, reject){

                request.post(requestOptions, function(error, response, body){
                    if (error != null ) {
                        return reject(error);
                    }
                    if (response.statusCode != 200) {
                        return reject(body);
                    }

                    resolve(body);
                });
            });
        },

        createUser: function(email, password) {

            var obj = {
                email: email,
                password: password
            }

            options = {
                "expiresIn" : "1 hour"
            };

            jwtToken = jwt.sign(obj, this.appKey, options);

            var requestOptions = {
                uri: AuthComponent.CLOUD_FUNCTIONS_URL + '/creates2authUser',
                json: true,
                body: {
                    requester: this.requester,
                    data: jwtToken
                },
                headers: {
                    'Content-Type' : 'application/json'
                }
            };

            return new Promise(function(resolve, reject) {
                request.post(requestOptions, function (error, response, body) {

                    if (error != null ) {
                        return reject(error);
                    }

                    if (response.statusCode != 200) {
                        return reject(body);
                    }

                    return resolve(body);
                });
            });
        }
    };

})();
module.exports = AuthComponent;
